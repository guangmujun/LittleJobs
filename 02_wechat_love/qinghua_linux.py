#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-01-19 15:52:31
# @Author  : 广慕君 (yuhangwang18@gmail.com)
# @Link    : https://gitee.com/guangmujun
# @Version : $Id$

import os
import sys
import time
import itchat
import datetime
# from selenium import webdriver
# from lxml import etree
# import requests
# import re

# reload(sys)
# sys.setdefaultencoding('utf-8')#解决python2编码问题

'''抓取情话'''
# def crawl_love_words():
# 	print('--正在抓取情话--')
# 	browser = webdriver.PhantomJS(executable_path="C:\\phantomjs-2.1.1-windows\\bin\\phantomjs.exe")
# 	url = "http://www.binzz.com/yulu2/3588.html"
# 	browser.get(url)
# 	html = browser.page_source
# 	Selector = etree.HTML(html)
# 	love_words_xpath_str = "//div[@id='content']/p/text()"
# 	love_words = Selector.xpath(love_words_xpath_str)
# 	for i in love_words:
# 		word = i.strip("\n\t\u3000\u3000").strip()
# 		with open("love_words.txt","a") as file:
# 			file.write(word + "\n")
# 	print("--抓取情话完成--")

'''抓取图片'''
# def crawl_love_image():
# 	print("--正在抓取图片--")
# 	for i in range(1,22):
# 		url = "http://tieba.baidu.com/p/3108805355?pn={}".format(i)
# 		response = requests.get(url)
# 		html = response.text
# 		pattern = re.compile(r'<div.*?class="d_post_content j_d_post_content.*?">.*?<img class="BDE_Image" src="(.*?)".*?>.*?</div>', re.S)
# 		image_url = re.findall(pattern, html)
# 		for j, data in enumerate(image_url):
# 			pics = requests.get(data)
# 			pic_path = '.\\images'
# 			mkdir(pic_path)
# 			fq = open(pic_path + '\\' + str(i) + "_" + str(j) + '.jpg', 'wb')  # 下载图片，并保存和命名
# 			fq.write(pics.content)
# 			fq.close()
# 	print("图片抓取完成")

'''发送消息'''
def send_news():
	#计算相恋天数
	inLoveDate = datetime.datetime(2018,12,31)
	todayDate = datetime.datetime.today()
	inLoveDays = (todayDate - inLoveDate).days #在一起的天数
	newYearDays = inLoveDays-35	#距离新年的天数
	print('在一起 %s 天，新年的 %s 天' %(inLoveDays,newYearDays))

	#获取情话
	# file_path = os.getcwd() + '\\' + love_word_path
	file_path = 'love_words.txt'
	with open(file_path, encoding='GB18030') as file:
		love_word = file.readlines()[newYearDays].split('：')[1]
		print(love_word)
		

	#微信端发送消息
	itchat.auto_login(enableCmdQR=2,hotReload=True)#不需要多次扫码登陆，二维码在命令行中显示
	my_friend = itchat.search_friends(name = u'Callisto')	#输入微信好友昵称Callisto/扎伊尔苏维埃之翼
	girlfriend = my_friend[0]["UserName"]

	message = """
	亲爱的紫琪宝贝：
	  2019年的第 {} 天~
	  我们在一起的 {} 天 ~
	  今天老王想对你说：

	{}
	  最后也是最重要的！
	""".format(str(newYearDays),str(inLoveDays),love_word)

	itchat.send(message,toUserName = girlfriend)
	print("--情话已发送--")

	pic_path = './images'
	files = os.listdir(pic_path)
	# i=0
	# for item in files:#进入到文件夹内，对每个文件进行循环遍历
	# 	os.rename(os.path.join(pic_path,item),os.path.join(pic_path,(str(i)+'.jpg')))#表示找到每个文件的绝对路径并进行拼接操作
	# 	i += 1
	files.sort(key=lambda x:int(x[:-4]))
	file = files[newYearDays]
	love_image_file = './images/' + file
	try:
		itchat.send_image(love_image_file,toUserName=girlfriend)
		print("--图片已发送--")
	except Exception as e:
		print(e)

'''创建文件夹'''
# def mkdir(path):
# 	folder = os.path.exists(path)
# 	if not folder:
# 		os.makedirs(path)
# 		print("--完成文件夹创建--")
# 	else:
# 		print("--保存图片中--")

'''主函数'''
def main():#threeTime时段的参数
	send_news()
	time.sleep(60)

if __name__ == '__main__':
	while True:
		curr_time = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
		love_time = curr_time.split(" ")[1]
		if love_time == "09:30:00":
			main()
		else:
			print("现在时间：" + love_time)