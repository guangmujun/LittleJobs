#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Date    : 2019-01-19 15:52:31
# @Author  : 广慕君 (yuhangwang18@gmail.com)
# @Link    : https://gitee.com/guangmujun
# @Version : $Id$

import os
import sys
import time
import itchat

reload(sys)
sys.setdefaultencoding('utf-8')#解决编码问题

def main(threeTime):#threeTime时段的参数
	send_news(threeTime)
	time.sleep(60)

def send_news(threeTime):
	itchat.auto_login(enableCmdQR=2,hotReload=True)#不需要多次扫码登陆，二维码在命令行中显示
	my_friend = itchat.search_friends(name = u'Callisto')	#输入微信好友昵称
	girlfriend = my_friend[0]["UserName"]

	message = """紫琪小姐：
{}！吃药了没有呀~
记得吃药、多喝热水~
想你爱你的老王""".format(threeTime)

	itchat.send(message,toUserName = girlfriend)
	print("消息已发送")


if __name__ == '__main__':
	while True:
		curr_time = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime())
		love_time = curr_time.split(" ")[1]
		if love_time == "08:30:00":
			main("早上好")
		elif love_time == "12:30:00":
			main("中午好")
		elif love_time == "18:30:00":
			main("晚上好")	
		else:
			print("现在时间：" + love_time)