# Python操作网页版Wechat

编写python脚本，部署到云服务器，每天固定时间操作微信向好友发送消息。



## 安装要求

安装itchat库

``pip install itchat``



## 知识补充

Linux命令后台运行

1. command &：命令后台运行，关掉终端后，命令终止

2. nohup command &:命令后台运行，关掉终端后，命令仍然运行
   ```
   nohup python app.py &
   ps -ef |grep 2102		#查看进程号
   ```




Linux中文字符编码问题

```
UnicodeDecodeError:'ascii' codec can't decode byte

import sys
reload(sys)
sys.setdefaultencoding('utf8')
```



## 测试

1. window 10 本地测试
  1. 扫码登陆后正常运行
2. Linux 云端测试
   1. 无法显示二维码
   2. 字符编码出问题
   3. 考虑命令后台运行



## 使用说明

windows系统：使用app.py

linux系统：使用app_linux.py



## 版本控制

该项目使用码云进行版本管理。您可以在https://gitee.com/guangmujun/xxx参看当前可用版本 



## 作者

广慕君