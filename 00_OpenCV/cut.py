#-*- coding: utf-8 -*-
import cv2

print('注意：')
print('1.视频文件放在项目根目录')
print('2.项目根目录下要新建images文件夹') 
videoName = input("请输入视频文件名称：") 
videoFps = int(input("请输入视频帧率(fps)：")) 
picNum = int(input("截取图片的时间间隔(s)：")) 

vc = cv2.VideoCapture(videoName) #读入视频文件
c=1
 
if vc.isOpened(): #判断是否正常打开
    rval , frame = vc.read()
    print('--打开视频成功--')
else:
    rval = False
    print('--打开视频失败--')
 
timeF = videoFps*picNum  #视频帧计数间隔频率

i=1
while rval:   #循环读取视频帧
    rval, frame = vc.read()
    if(c%timeF == 0): #每隔timeF帧进行存储操作
        cv2.imwrite('./images/' + '%04d' %i + '.jpg',frame) #存储为图像
        i = i + 1
    c = c + 1
    cv2.waitKey(1)
vc.release()
print('--图片生成成功--')

 
