# OpenCV神技——人脸检测、猫脸检测

利用OpenCV的Python接口实现人脸检测和猫脸检测，检测时使用OpenCV自带的Haar特征检测。 



## 所用工具

1. python
2. OpenCV人脸检测 



## 知识补充

OpenCV

- 一个基于BSD许可（开源）发行的跨平台计算机视觉库 ,由一系列 C 函数和少量 C++ 类构成，同时提供了Python、Ruby、MATLAB等语言的接口，实现了图像处理和计算机视觉方面的很多通用算法。 
- 官方网址为：https://opencv.org/， Github：https://github.com/opencv 
- Haar特征检测中训练好的模型： https://github.com/opencv/opencv/tree/master/data/haarcascades，其中：
  - 猫脸检测：haarcascade_frontalcatface.xml
  - 人脸检测：haarcascade_frontalface_default.xml 

## 注意点

1. 安装库文件

   对于```import cv2```而言，应该```pip install opencv-python```，而不是```pip install cv2```

2. cv2.error: OpenCV(3.4.2)错误

   图片路径汇总文件夹分隔符使用错误造成，正确用法如下：

   ```ImagePath = './data/james.jpg'```

## 开发流程

1. 人脸检测

   1. 读取图片
   2. 将图片转换为灰度模式，便于人脸检测 
   3. 利用Haar特征检测图片中的人脸 
   4. 绘制人脸的矩形区域 
   5. 显示人脸检测后的图片 

2. 猫脸检测

   与人脸检测同理，将训练好的模型换为haarcascade_frontalcatface.xml即可。



## 测试

1. 人脸检测

![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542447445608.png) 

![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542447478565.png)

![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542447389965.png)

![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542447360985.png)



2. 猫脸检测

![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542448217970.png)



![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542448383977.png)



![Alt text](https://gitee.com/guangmujun/LittleJobs/raw/master/00_OpenCV/data/results/1542448458137.png)



3. 测试结果：

- 测试中使用的图片尺寸适中偏小,人脸或者猫脸部分较为清晰。人脸检测的准确度明显高于猫脸检测，
- 当图片中的场景稍复杂或者个体的数量稍多时，都无法较为准确地检测出来。
- 所以，这里只适用于简单场景、少数个体的人脸或者猫脸检测。

## 拓展

结合深度学习的模型如CNN，实现：

- 人脸识别
- 物体检测
- 自动给图像打标签 



## 使用说明

1. 安装OpenCV库文件```pip install opencv-python```
2. 修改代码文件中的图片路径后，即可运行。

```
# 待检测的图片路径
ImagePath = './data/zxy2.jpg'
```





## 版本控制

该项目使用码云进行版本管理。您可以在https://gitee.com/guangmujun/LittleJobs 参看当前可用版本 



## 作者

Python中文社区 、数据科学俱乐部 、广慕君